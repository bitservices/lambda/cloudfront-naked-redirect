"use strict";
exports.handler = (event, context, callback) => {

  // Extract the request & response from the CloudFront event that is sent to Lambda@Edge
  var request = event.Records[0].cf.request;

  // Save any query string parameters
  var params = "";
  if (("querystring" in request) && (request.querystring.length > 0)) {
    params = "?" + request.querystring;
  }

  // Save the URI
  var uri = "/";
  if (("uri" in request) && (request.uri.length > 0)) {
    uri = request.uri;
  }

  // Extract the headers from the request
  var headers = request.headers;

  // Extract the host header from the headers
  var host = "";
  if (("host" in headers) && (headers.host.length > 0)) {
    if (("value" in headers.host[0]) && (headers.host[0].value.length > 0)) {
      host = headers.host[0].value;
    }
  }

  // Check if host is already a 'www' subdomain
  var www_regex = /^www\..+\..+$/;

  if (host.length > 0) {
    if (!(www_regex.test(host))) {
      // Detected a valid host header, that is not to the www subdomain
      var newuri = "https://www." + host + uri + params;

      // Construct Redirect
      const response = {
        status: "301",
        statusDescription: "Moved Permanently",
        headers: {
          location: [{
            key: "Location",
            value: newuri
          }]
        }
      };

      // Send Redirect
      return callback(null, response);
    }
  }

  // Carry on as normal
  return callback(null, request);
};
